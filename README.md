This project was bootstrapped with [Create React App](https://github.com/facebook/create-react-app) and --TypeScript.

## Overview

Hi! I'm Sean and this is is the solution to the logbook code refactor challenge from Uptick HQ.

Please feel free to clone this codebase: https://bitbucket.org/Nostin/uptick/src/master/.

Navigate to the root directory and type `yarn` to install the dependencies.  `npm install` should also be fine if you prefer it to yarn.

This project uses typescript so if you don't use [Visual Studio Code](https://code.visualstudio.com/), make sure you have TypeScript intellisense installed into your IDE of choice for maximum viewing pleasure.

In the project directory, you can run:
### `yarn start`

Runs the app in the development mode.<br>
Open [http://localhost:3000](http://localhost:3000) to view it in the browser.

The page will reload if you make edits.
You will also see any lint errors in the console.
### `yarn test`

Launches the test runner in the interactive watch mode.
See the section about [running tests](https://facebook.github.io/create-react-app/docs/running-tests) for more information.

## Existing Code Review

The original code I was asked to refactor is in the root directory with the filename called **logbook-entry-list.jsx**

Running our eye over this file we might note the following concerns:

- This file is doing quite a lot of stuff.  It's over 500 lines long with three components inside so it takes a bit of digging to figure out what's going on.

- On line 105 there is some inline styling going on.  We'd like to abstract that out into the stylesheet if we can avoid things like this.

- Lines 143, 268, 340 have &nbsp; forcing whitespace between elements.  Again this is presentational and could be better served in the stylesheet.

- Line 184 is making use of the deprecated lifecycle method 'componentWillUseProps'. Pretty soon once React hits version 17 this method will no longer be supported, so ideally we'd like to avoid using it.

- Lines 107, 121, 305, 319 make use of refs.  We generally want to avoid using refs because it breaks encapsulation and can cause parts of the virtual DOM to re-render that don’t need to be, impacting performance.  The official React docs say to avoid using them if you can do it declaratively.  i.e use props to talk to child components so when we refactor we'll strip this out too.

- Again on the performance front, I don’t see any deep state hierarchy that needs to be managed, so therefore the better performing **PureComponent** could be used for the three components in this file.  This will only perform a shallow comparison of props and state to force e rerender and is a bit quicker than its **Component** counterpart.

- Class methods are all using arrow functions.  This isn’t a huge deal, but whenever the browser executes an arrow function it creates a new function object, so this can cause performance decrease.  Personally I like to keep my class methods on the prototype and bind them in the constructor to avoid this.  It also makes them easier to test for example if I wanted to spy on a specific method if it's on the prototype.

- On that note, I'm not sure where I'd even begin writing tests for this file! I rather suspect there probably isn't any :P

- I feel like clarity could be improved with what's goin on in this file.  For example there's an API call on line 68 and I've had to really dig in just to see what it's doing.  Also the name of the first component is called 'FirstCardBlock' but it actually looks like it's a form that adds new cards to an existing set of cards.  We could improve the naming conventions here.

- Final thoughts: This file has loads going on, mixing up presentation, state management, and even api calls all together.  There are three components exported in the one file and there's rendering of bits and pieces that could have easily been their own component for better re-use, ease of management, and clarity of meaning.

## The Plan

I think we can improve what we have here in the following key ways:

1. Modularise the code.  I like to use a structure called **block** and **pattern** components.  It's a similar idea to Dan Abramov's 'presentational' and 'functional' structure.  For things like date pickers and auto completes or even just buttons and text fields, let's abstract them out into blocks.  These are fairly small and simple and easy to reuse across this and even other projects in the business.  We can then just import them as and when they're needed.  They'll be much simpler to test and we can do things like make them responsive or even screen-reader compliant if we want.  I'll pop them in /src/components/blocks for you to check out.


    Incidentally, **Pattern** components are bigger modules of things more likely to just be useful in this specific instance, for example the Add New Entry Card form which will import a series of our block components.

2. Abstract state management away.  The application state and the data used by the app doesn't need to be so tightly coupled with the components.  We can abstract all of this away so the components themselves can concentrate on 'reacting' to the changing data.  This includes the API calls which is just another mechanism for fetching and setting data/state.  This helps if other components want to access the same data or API call, it can just be passed to them through a prop.

3. Improving the code base.  The following measures will result in a more manageable, more stable, clearer, easier to work with code base:

    - split code into appropriate components
    - abstract away state from the components
    - abstract styling away from the components
    - apply adequate unit testing coverage
    - sensible naming conventions
    - refactoring out things like deprecated lifecycle methods, not using refs, no forced whitespace

4. Process tooling.  Aside from the refactoring of the actual code as set out above, I've also employed some tooling to provide some standards and consistency, such as prettier, typescript, and linting.  This will help keep the code base cleaner and less prone to errors going forward.

## Dependencies

I've added some dependencies to this project to help achieve what is set out above.  In general it's best to avoid adding dependencies unnecessarily which then need to be maintained unless they are being used for good reason.  Listed here are the dependencies used and why they're a good idea.

**Redux**

Super popular state management tool useful for state management within React applications.  Since state management is something that needs to be handled regardless, it makes sense to use a library like Redux that is both excellent at doing just that and is widely used in this context.

**Thunk**

Simple middleware to allow Redux actions to return actions as well as objects which is invaluable when abstracting things like API calls to Redux actions rather than have them for example in a React componentDidMount method.

**Immutability Helper**

One downside to state management in React applications using javascript objects is that it can be easy to inadvertently mutate state which can cause bugs that are difficult to diagnose.  Immutability Helper helps prevent this and additionally helps reduce code duplication in Redux reducers when returning the updated state.

**Enzyme, FetchMock, ReduxMockStore**

Libraries to help make interacting with components and state easier when unit testing.

**TypeScript**

TypeScript is a super-set of javascript and is getting super popular in the front-end world.  It's basically javascript plus types.  This makes debugging much easier and prevents a lot of common bugs when you're actually writing code.  In my opinion it also gives much greater visibility and clarity on exactly what is being passed around.  For example in the Redux reducers you have actual typed state which you can look at and understand exactly what that state looks like, rather than just have a weakly typed JS object floating outside of your react components.  It's good and for JS developers very easy to learn.

**ESLint, TSLint, Prettier**

To help with point 4 of the plan.  We all have our different preferences and ways of coding.  These tools help structure code in a specific, sensible, best practice kind of way.  They demonstrate how we can enforce some standards and structure to the code base so that all the devs working on it write as similar as possible.  It makes the code you inherit or pass on feel so much more familiar, and also helps prevent you from causing common issues.

**React-Autocomplete, React-Datepicker**

Just standard libraries to provide basic auto-complete and date-picker functionality since it looks like this sort of thing was being used.  I'm not married to these - I'm sure you have preferences for other ones but these ones are useful to just demonstrate the functionality described above.

## Notes

I haven't gone too nuts on this, just enough to demonstrate the solution so I've just implemented the add new card form.  Obviously I don't have uptick's API url so the call is going to fail, I've shown a working API call below the form to demonstrate how it would work if the real api url existed.

Things like css are also left pretty basic.  Some very basic responsiveness is there but I haven't used any preprocessors like SASS or anything.