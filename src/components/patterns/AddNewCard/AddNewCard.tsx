import React from 'react';
import { connect } from 'react-redux';
import {
  showCreate,
  showNew,
  submitNewLogbookEntry,
  updateLogBookEntryNotes,
  updateLogBookEntryServiceDate,
  updateLogBookEntryTask,
} from '../../../redux/actions/newLogbookEntryAction';
import { NewLogbookEntryState } from '../../../redux/reducers/newLogbookEntryReducer';
import AutoCompleteField from '../../blocks/AutoCompleteField/AutoCompleteField';
import Button from '../../blocks/Button/Button';
import DateField from '../../blocks/DateField/DateField';
import LoadingSpinner from '../../blocks/Spinner/LoadingSpinner';
import TextArea from '../../blocks/TextArea/TextArea';
import './add-new-card.css';

interface IProps {
  url?: string;
  logbookId: number;
  propertyId?: number;
  pushNew?: (entry: object) => void;
  newLogbookEntry: NewLogbookEntryState;
  rdxShowCreate: () => void;
  rdxShowNew: () => void;
  rdxSubmitNewLogbookEntry: (
    logbookId: number,
    notes: string,
    serviceDate: Date,
    task: string
  ) => void;
  rdxUpdateLogBookEntryNotes: (text: string) => void;
  rdxUpdateLogBookEntryServiceDate: (text: Date) => void;
  rdxUpdateLogBookEntryTask: (text: string) => void;
}

class AddNewCard extends React.PureComponent<IProps, any> {
  constructor(props: any) {
    super(props);

    this.handleServiceDateChange = this.handleServiceDateChange.bind(this);
    this.handleNotesChange = this.handleNotesChange.bind(this);
    this.handleTaskChange = this.handleTaskChange.bind(this);
    this.showNewEntry = this.showNewEntry.bind(this);
    this.cancelSaveNewEntry = this.cancelSaveNewEntry.bind(this);
    this.handleSubmit = this.handleSubmit.bind(this);
  }

  public render() {
    const { newLogbookEntry } = this.props;

    const showSpinner = newLogbookEntry.apiStatus === 'active';
    const showApiFailMsg = newLogbookEntry.apiCallResult === 'fail';
    const showApiSuccessMsg = newLogbookEntry.apiCallResult === 'success';

    if (newLogbookEntry.state === 'new') {
      return (
        <div className="add-new-card">
          <Button type="positive" text="Add Logbook Entry" onClick={this.showNewEntry} />
        </div>
      );
    } else {
      return (
        <div className="add-new-card">
          <h3>New logbook entry</h3>
          <DateField
            label="Service Date"
            onChange={this.handleServiceDateChange}
            value={newLogbookEntry.serviceDate}
          />
          <TextArea
            label="Notes (optional)"
            onChange={this.handleNotesChange}
            optional={true}
            placeholder="Notes"
            value={newLogbookEntry.notes}
          />
          <AutoCompleteField
            items={newLogbookEntry.taskOptions}
            label="Task (optional)"
            onSelect={this.handleTaskChange}
            optional={true}
            placeholder="Type here to view tasks..."
          />
          {showApiFailMsg && <p className="error">Unable to add the new entry.</p>}
          {showApiSuccessMsg && <p className="notification">New entry added.</p>}
          {showSpinner && <LoadingSpinner />}
          <div>
            <Button type="negative" text="Cancel" onClick={this.cancelSaveNewEntry} />
            <Button type="positive" text="Save" onClick={this.handleSubmit} />
          </div>
        </div>
      );
    }
  }

  private cancelSaveNewEntry() {
    const { rdxShowNew } = this.props;
    rdxShowNew();
  }

  private showNewEntry() {
    const { rdxShowCreate } = this.props;
    rdxShowCreate();
  }

  private handleServiceDateChange(text: Date) {
    const { rdxUpdateLogBookEntryServiceDate } = this.props;
    rdxUpdateLogBookEntryServiceDate(text);
  }

  private handleNotesChange(text: string) {
    const { rdxUpdateLogBookEntryNotes } = this.props;
    rdxUpdateLogBookEntryNotes(text);
  }

  private handleTaskChange(text: string) {
    const { rdxUpdateLogBookEntryTask } = this.props;
    rdxUpdateLogBookEntryTask(text);
  }

  private handleSubmit(event: React.MouseEvent<HTMLElement, MouseEvent>) {
    event.preventDefault();
    const { logbookId, newLogbookEntry, rdxSubmitNewLogbookEntry } = this.props;
    rdxSubmitNewLogbookEntry(
      logbookId,
      newLogbookEntry.notes,
      newLogbookEntry.serviceDate,
      newLogbookEntry.task
    );
  }
}

const mapStateToProps = (state: { [property: string]: NewLogbookEntryState }) => ({
  newLogbookEntry: state.newLogbookEntryReducer,
});

const mapDispatchToProps = (dispatch: any) => ({
  rdxShowCreate: () => dispatch(showCreate()),
  rdxShowNew: () => dispatch(showNew()),
  rdxSubmitNewLogbookEntry: (logbookId: number, notes: string, serviceDate: Date, task: string) =>
    dispatch(submitNewLogbookEntry(logbookId, notes, serviceDate, task)),
  rdxUpdateLogBookEntryNotes: (text: string) => dispatch(updateLogBookEntryNotes(text)),
  rdxUpdateLogBookEntryServiceDate: (text: Date) => dispatch(updateLogBookEntryServiceDate(text)),
  rdxUpdateLogBookEntryTask: (text: string) => dispatch(updateLogBookEntryTask(text)),
});

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(AddNewCard);
