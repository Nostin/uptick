import React, { PureComponent } from 'react';
import DatePicker from 'react-datepicker';
import 'react-datepicker/dist/react-datepicker.css';
import './date-field.css';

interface Props {
  label?: string;
  onChange: (date: Date) => void;
  optional?: boolean;
  value?: Date;
}

class DateField extends PureComponent<Props> {
  constructor(props: Props) {
    super(props);

    this.handleDateChange = this.handleDateChange.bind(this);
  }

  public render() {
    const { label, optional, value } = this.props;

    return (
      <label className="date-field">
        <p>
          {label}
          {!optional && <span>*</span>}
        </p>
        <DatePicker onChange={this.handleDateChange} selected={value} />
      </label>
    );
  }

  private handleDateChange(date: Date) {
    const { onChange } = this.props;
    onChange(date);
  }
}

export default DateField;
