import { mount, shallow } from 'enzyme';
import React from 'react';
import DateField from './DateField';

describe('DateField component test suite', () => {
  const mockChangeFn = jest.fn();
  const today = new Date();
  const todayFormat =
    ('0' + (today.getMonth() + 1)).slice(-2) +
    '/' +
    ('0' + today.getDate()).slice(-2) +
    '/' +
    today.getFullYear();
  const componentRender = () => (
    <DateField label="Date Label" onChange={mockChangeFn} value={today} />
  );

  const componentRenderOptional = () => (
    <DateField label="Date Label" onChange={mockChangeFn} optional={true} value={today} />
  );

  it('Should display a label with the label text from the prop', () => {
    const datefld = shallow(componentRenderOptional());
    expect(datefld.find('p').text()).toEqual('Date Label');
  });

  it('Should render only the default text supplied in the prop', () => {
    const datefld = mount(componentRender());
    expect(datefld.find('input').props().value).toEqual(todayFormat);
  });

  it('Should have the mandatory asterisk displayed as the optional prop was omitted', () => {
    const datefld = shallow(componentRender());
    expect(datefld.find('p span').length).toEqual(1);
  });

  it('Should call onChange function when date picker date is clicked', () => {
    const datefld = mount(componentRenderOptional());
    expect(datefld.find('.react-datepicker__day').length).toEqual(0);
    datefld.find('input').simulate('focus');
    datefld.update();
    datefld
      .find('.react-datepicker__day')
      .at(10)
      .simulate('click');
    expect(mockChangeFn).toHaveBeenCalled();
  });
});
