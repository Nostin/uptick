import React from 'react';
import './spinner.css';

const LoadingSpinner = () => (
  <div className="loading-spinner">
    <p>Your content is being loaded...</p>
  </div>
);

export default LoadingSpinner;
