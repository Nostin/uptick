import { shallow } from 'enzyme';
import React from 'react';
import LoadingSpinner from './LoadingSpinner';

describe('Loading Spinner rendering ok', () => {
  it('Renders the Loading Spinner component correctly', () => {
    const wrapper = shallow(<LoadingSpinner />);
    expect(wrapper).toMatchSnapshot();
  });
});
