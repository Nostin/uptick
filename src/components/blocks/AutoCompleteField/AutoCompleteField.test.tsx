import { mount, shallow } from 'enzyme';
import React from 'react';
import AutoCompleteField from './AutoCompleteField';

describe('AutoComplete component test suite', () => {
  const mockSelectFn = jest.fn();
  const componentRender = () => (
    <AutoCompleteField
      items={[{ key: 'id1', label: 'one' }, { key: 'id2', label: 'two' }]}
      label="test label"
      onSelect={mockSelectFn}
      optional={false}
      placeholder="Type here to view tasks..."
      value="default text"
    />
  );

  const componentRenderOptional = () => (
    <AutoCompleteField
      items={[{ key: 'id1', label: 'one' }, { key: 'id2', label: 'two' }]}
      label="test label"
      onSelect={mockSelectFn}
      optional={true}
      placeholder="Type here to view tasks..."
    />
  );

  it('Renders the TextField component correctly', () => {
    const autocompletefld = shallow(componentRender());
    expect(autocompletefld).toMatchSnapshot();
  });

  it('Should display a label with the label text from the prop', () => {
    const autocompletefld = shallow(componentRenderOptional());
    expect(autocompletefld.find('p').text()).toEqual('test label');
  });

  it('Should render only the default text supplied in the prop', () => {
    const autocompletefld = mount(componentRender());
    expect(autocompletefld.find('input').props().value).toEqual('default text');
    expect(autocompletefld.find('input').props().placeholder).toEqual('Type here to view tasks...');
  });

  it('Should have the mandatory asterisk displayed as the optional prop was omitted', () => {
    const autocompletefld = shallow(componentRender());
    expect(autocompletefld.find('p span').length).toEqual(1);
  });

  it('Should call onSelect function when autocomplete selection is clicked', () => {
    const autocompletefld = mount(componentRenderOptional());
    expect(autocompletefld.find('.auto-complete-item').length).toEqual(0);
    autocompletefld.find('input').simulate('focus');
    autocompletefld.update();
    expect(autocompletefld.find('.auto-complete-item').length).toEqual(2);
    autocompletefld
      .find('.auto-complete-item')
      .first()
      .simulate('click');
    expect(mockSelectFn).toHaveBeenCalled();
  });
});
