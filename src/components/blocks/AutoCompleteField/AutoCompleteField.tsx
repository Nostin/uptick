import React, { PureComponent } from 'react';
import AutoComplete from 'react-autocomplete';
import './auto-complete.css';

interface Props {
  items: SelectItem[];
  label?: string;
  onChange?: (text: string) => void;
  onSelect: (text: string) => void;
  optional?: boolean;
  placeholder?: string;
  value?: string;
}

interface IState {
  txtvalue?: string;
}

interface SelectItem {
  label: string;
  key: string;
}

class AutoCompleteField extends PureComponent<Props, IState> {
  constructor(props: Props) {
    super(props);

    this.state = {
      txtvalue: '',
    };

    this.change = this.change.bind(this);
    this.select = this.select.bind(this);
    this.filterSelectOptions = this.filterSelectOptions.bind(this);
    this.itemDisplayValue = this.itemDisplayValue.bind(this);
    this.renderItem = this.renderItem.bind(this);
  }

  public render() {
    const { label, optional, items, placeholder, value } = this.props;
    const { txtvalue } = this.state;

    const val = txtvalue ? txtvalue : value;

    return (
      <label className="auto-complete">
        <p>
          {label}
          {!optional && <span>*</span>}
        </p>
        <AutoComplete
          getItemValue={this.itemDisplayValue}
          items={items}
          renderItem={this.renderItem}
          value={val}
          onChange={this.change}
          onSelect={this.select}
          shouldItemRender={this.filterSelectOptions}
          inputProps={{ placeholder }}
        />
      </label>
    );
  }

  private change(event: React.ChangeEvent<HTMLInputElement>) {
    this.setState({ txtvalue: event.target.value });
  }

  private select(text: string) {
    const { onSelect } = this.props;
    this.setState({ txtvalue: text });
    onSelect(text);
  }

  private itemDisplayValue(item: SelectItem) {
    return item.label;
  }

  private filterSelectOptions(item: SelectItem, value: string) {
    return item.label.toLowerCase().indexOf(value.toLowerCase()) > -1;
  }

  private renderItem(item: SelectItem, isHighlighted: boolean) {
    const cls = isHighlighted ? 'auto-complete-item highlight' : 'auto-complete-item';
    return (
      <div className={cls} key={item.key}>
        {item.label}
      </div>
    );
  }
}

export default AutoCompleteField;
