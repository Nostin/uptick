import { mount, shallow } from 'enzyme';
import React from 'react';
import TextArea from './TextArea';

describe('TextArea component test suite', () => {
  const mockChangeFn = jest.fn();
  const componentRender = () => (
    <TextArea
      label="Mandatory Text Field"
      onChange={mockChangeFn}
      optional={false}
      placeholder="placeholder text"
      value="default text"
    />
  );

  const componentRenderOptional = () => (
    <TextArea
      label="Mandatory Text Field"
      onChange={mockChangeFn}
      optional={true}
      value="default text"
    />
  );

  it('Renders the TextArea component correctly', () => {
    const textarea = shallow(componentRender());
    expect(textarea).toMatchSnapshot();
  });

  it('Should display a label with the label text from the prop', () => {
    const textarea = shallow(componentRenderOptional());
    expect(textarea.find('p').text()).toEqual('Mandatory Text Field');
  });

  it('Should render only the default text supplied in the prop', () => {
    const textarea = mount(componentRender());
    expect(textarea.find('textarea').text()).toEqual('default text');
    expect(textarea.find('textarea').props().placeholder).toEqual('placeholder text');
  });

  it('Should have the mandatory asterisk displayed as the optional prop was omitted', () => {
    const textarea = shallow(componentRender());
    expect(textarea.find('p span').length).toEqual(1);
  });

  it('Should call onChange function when text is entered', () => {
    const textarea = mount(componentRender());
    textarea.find('textarea').simulate('change', { target: { value: 'test' } });
    expect(mockChangeFn).toHaveBeenCalled();
  });
});
