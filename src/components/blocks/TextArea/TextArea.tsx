import React, { PureComponent } from 'react';
import './textarea.css';

interface Props {
  label?: string;
  onChange: (text: string) => void;
  optional?: boolean;
  placeholder?: string;
  value?: string;
}

class TextArea extends PureComponent<Props> {
  constructor(props: Props) {
    super(props);

    this.change = this.change.bind(this);
  }

  public render() {
    const { label, optional, placeholder, value } = this.props;

    return (
      <label className="text-area">
        <p>
          {label}
          {!optional && <span>*</span>}
        </p>
        <textarea onChange={this.change} defaultValue={value} placeholder={placeholder} />
      </label>
    );
  }

  private change(event: React.ChangeEvent<HTMLTextAreaElement>) {
    const { onChange } = this.props;
    onChange(event.target.value);
  }
}

export default TextArea;
