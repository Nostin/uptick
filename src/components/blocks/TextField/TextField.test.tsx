import { mount, shallow } from 'enzyme';
import React from 'react';
import TextField from './TextField';

describe('TextField component test suite', () => {
  const mockChangeFn = jest.fn();
  const componentRender = () => (
    <TextField
      label="Mandatory Text Field"
      onChange={mockChangeFn}
      optional={false}
      value="default text"
    />
  );

  const componentRenderOptional = () => (
    <TextField
      label="Mandatory Text Field"
      onChange={mockChangeFn}
      optional={true}
      value="default text"
    />
  );

  it('Renders the TextField component correctly', () => {
    const textfield = shallow(componentRender());
    expect(textfield).toMatchSnapshot();
  });

  it('Should display a label with the label text from the prop', () => {
    const textfield = shallow(componentRenderOptional());
    expect(textfield.find('p').text()).toEqual('Mandatory Text Field');
  });

  it('Should render only the default text supplied in the prop', () => {
    const textfield = mount(componentRender());
    expect(textfield.find('input[type="text"]').props().defaultValue).toEqual('default text');
  });

  it('Should have the mandatory asterisk displayed as the optional prop was omitted', () => {
    const textfield = shallow(componentRender());
    expect(textfield.find('p span').length).toEqual(1);
  });

  it('Should call onChange function when text is entered', () => {
    const textfield = mount(componentRender());
    textfield.find('input[type="text"]').simulate('change', { target: { value: 'test' } });
    expect(mockChangeFn).toHaveBeenCalled();
  });
});
