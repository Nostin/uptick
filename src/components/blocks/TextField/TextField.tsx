import React, { PureComponent } from 'react';
import './text-field.css';

interface Props {
  label?: string;
  onChange: (text: string) => void;
  optional?: boolean;
  value?: string;
}

class TextField extends PureComponent<Props> {
  constructor(props: Props) {
    super(props);

    this.change = this.change.bind(this);
  }

  public render() {
    const { label, optional, value } = this.props;

    return (
      <label className="text-field">
        <p>
          {label}
          {!optional && <span>*</span>}
        </p>
        <input type="text" onChange={this.change} defaultValue={value} />
      </label>
    );
  }

  private change(event: React.ChangeEvent<HTMLInputElement>) {
    const { onChange } = this.props;
    onChange(event.target.value);
  }
}

export default TextField;
