import React, { PureComponent } from 'react';
import './button.css';

interface Props {
  text: string;
  type: 'positive' | 'negative';
  onClick: (event: React.MouseEvent<HTMLElement>) => void;
}

class Button extends PureComponent<Props> {
  constructor(props: Props) {
    super(props);

    this.click = this.click.bind(this);
  }

  public render() {
    const { text, type } = this.props;

    return (
      <button className={`button ${type}`} onClick={this.click}>
        {text}
      </button>
    );
  }

  private click(event: React.MouseEvent<HTMLElement>) {
    const { onClick } = this.props;
    onClick(event);
  }
}

export default Button;
