import { mount, shallow } from 'enzyme';
import React from 'react';
import Button from './Button';

describe('Button component test suite', () => {
  const mockClickFn = jest.fn();
  const componentRender = () => <Button type="negative" text="Cancel" onClick={mockClickFn} />;

  it('Renders the Button component correctly', () => {
    const button = shallow(componentRender());
    expect(button).toMatchSnapshot();
  });

  it('Should render only the button text supplied in the prop', () => {
    const button = shallow(componentRender());
    expect(button.text()).toEqual('Cancel');
  });

  it('Should have the .negative class name as passed in by the prop', () => {
    const button = shallow(componentRender());
    expect(button.hasClass('negative')).toEqual(true);
  });

  it('Should call onClick function when the button is clicked', () => {
    const button = mount(componentRender());
    button.simulate('click');
    expect(mockClickFn).toHaveBeenCalled();
  });
});
