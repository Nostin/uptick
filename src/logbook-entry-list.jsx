import PropTypes from 'prop-types'
import React from 'react'
import ApiUtils from 'apps/core/frontend/utils/api'
import Moment from 'moment'
import AutocompleteLight from 'apps/core/frontend/widgets/autocomplete-light'

import { hasPerm } from 'apps/core/frontend/context.jsx'

import {DateInput} from 'apps/core/frontend/widgets/datetimes/datetimes'

import FileBrowser, { FileRenderers, FolderRenderers } from '@uptickhq/web-common/src/file-browser'

const GUI_FORMAT = 'DD/MM/YYYY'
const API_FORMAT = 'YYYY-MM-DD'

class FirstCardBlock extends React.Component {
  static propTypes = {
    url: PropTypes.string,
    logbookId: PropTypes.number,
    propertyId: PropTypes.number,
    pushNew: PropTypes.func,
  }

  baseState = {
    state: 'new',
    serviceDate: Moment(),
    notes: '',
  }

  state = this.baseState

  newEntryOnClick = (event) => {
    event.preventDefault()
    this.setState({state: 'create'})
  }
  cancelCreateOnClick = (event) => {
    event.preventDefault()
    this.setState(this.baseState)
  }
  handleSubmit = (event) => {
    event.preventDefault()

    const payload = {
      data: {
        type: 'LogbookEntry',
        attributes: {
          service_date: this.state.serviceDate.format(API_FORMAT),
          notes: this.state.notes,
        },
        relationships: {
          logbook: {
            data: {
              type: 'Logbook',
              id: this.props.logbookId,
            },
          },
          task: {
            data: this.state.task ? {
              type: 'Task',
              id: this.state.task.value,
            } : null,
          },
        },
      },
    }

    const postURL = `${this.props.url}?include=task`
    ApiUtils.httpPOST({
      autoPrefix: false,
      url: postURL,
      data: payload,
      contentType: 'application/vnd.api+json',
      success: (response) => {
        this.setState(this.baseState)
        this.props.pushNew(ApiUtils.joinIncludes(response).data)
      },
    })
  }
  handleServiceDateChange = (selectedDate) => {
    this.setState({serviceDate: Moment(selectedDate, 'x')})
  }
  handleNotesChange = () => {
    this.setState({notes: this.refs.notes.value})
  }
  handleTaskChange = (value) => {
    this.setState({task: value})
  }

  render() {
    let content
    if (this.state.state === 'new') {
      content = (
        <div className="text-xs-right actions">
          <a className="btn btn-sm btn-primary" onClick={this.newEntryOnClick} href="#">
            <i className="far fa-file-alt" /> Add logbook entry
          </a>
        </div>
      )
    } else if (this.state.state === 'create') {
      content = (
        <form method="POST" action="" onSubmit={this.handleSubmit}>
          <h5>New logbook entry</h5>
          <div className="form-group required">
            <label className="control-label">Service date</label>
            <div style={{position: 'relative'}}>
              <DateInput
                ref="serviceDate"
                value={Moment(this.state.serviceDate, GUI_FORMAT).format(GUI_FORMAT)}
                placeholder="Service date"
                required
                onChange={this.handleServiceDateChange}
              />
              <div className="alert alert-danger" hidden={this.state.serviceDate.isValid()}>
                <p>Invalid date. Expected DD/MM/YYYY.</p>
              </div>
            </div>
          </div>
          <div className="form-group">
            <label className="control-label">Notes (optional)</label>
            <textarea
              ref="notes"
              className="form-control"
              name="notes"
              placeholder="Notes"
              value={this.state.notes}
              onChange={this.handleNotesChange}
            />
          </div>
          <div className="form-group">
            <label className="control-label">Task (optional)</label>
            <AutocompleteLight
              onChange={this.handleTaskChange}
              value={this.state.task}
              endpoint={'task/?property=' + this.props.propertyId}
              placeholder="Type to search tasks ..."
              clearable
            />
          </div>
          <div className="pull-xs-right">
            <button className="btn btn-outline-warning btn-sm" onClick={this.cancelCreateOnClick}>
              Cancel
            </button>
            &nbsp;
            <button
              type="submit"
              className="btn btn-primary btn-sm"
              disabled={!this.state.serviceDate.isValid()}
            >
              Save
            </button>
          </div>
        </form>
      )
    }
    return (
      <div className="card-block">
        {content}
      </div>
    )
  }
}

class OtherCardBlock extends React.Component {
  static propTypes = {
    id: PropTypes.number,
    propertyId: PropTypes.number,
    attributes: PropTypes.object,
    relationships: PropTypes.object,
    url: PropTypes.string,
    updateEntry: PropTypes.func,
    deleteEntry: PropTypes.func,
    task: PropTypes.object,
  }

  baseState = {
    state: 'detail',
    serviceDate: Moment(this.props.attributes.service_date, API_FORMAT),
    notes: this.props.attributes.notes,
    task: this.props.task,
  }

  state = {...this.baseState}

  componentWillReceiveProps(nextProps) {
    if (this.state.state !== 'edit') {
      this.setState({
        serviceDate: Moment(nextProps.attributes.service_date, API_FORMAT),
        notes: nextProps.attributes.notes,
      })
    }
  }
  handleTaskChange = (value) => {
    this.setState({task: value})
  }
  editOnClick = (event) => {
    event.preventDefault()
    this.setState({state: 'edit'})
  }
  cancelEditOnClick = (event) => {
    event.preventDefault()
    this.setState(this.baseState)
  }
  addPhotosOnClick = (event) => {
    event.preventDefault()
    this.setState({state: 'add_photos'})
  }
  deleteOnClick = (event) => {
    ApiUtils.httpDELETE({
      autoPrefix: false,
      url: this.props.url,
      data: {
        type: 'LogbookEntry',
        id: this.props.id,
      },
      contentType: 'application/vnd.api+json',
      success: (response) => {
        this.setState({state: 'detail'})
        this.props.deleteEntry(this.props.id)
      },
    })
  }
  handleSubmit = (event) => {
    event.preventDefault()
    const payload = {
      data: {
        type: 'LogbookEntry',
        id: this.props.id,
        attributes: {
          service_date: this.state.serviceDate.format(API_FORMAT),
          notes: this.state.notes,
        },
        relationships: {
          task: {
            data: {
              type: 'Task',
              id: this.state.task.value,
            },
          },
        },
      },
    }
    const patchURL = `${this.props.url}?include=task`
    ApiUtils.httpPATCH({
      autoPrefix: false,
      url: patchURL,
      data: payload,
      contentType: 'application/vnd.api+json',
      success: (response) => {
        this.setState({state: 'detail'})
        this.props.updateEntry(this.props.id, ApiUtils.joinIncludes(response).data)
      },
    })
  }
  handleServiceDateChange = (selectedDate) => {
    this.setState({serviceDate: Moment(selectedDate, 'x')})
  }
  handleNotesChange = () => {
    const newValue = this.refs.notes.value
    this.setState({notes: newValue})
  }

  render() {
    let task = ''
    const inEditMode = this.state.state === 'add_photos'
    if (this.props.relationships.task && this.props.relationships.task.data) {
      task = (
        <h5>
          Task:&nbsp;
          <a href={this.props.relationships.task.data.attributes.get_absolute_url}>
            {this.props.relationships.task.data.attributes.name}
          </a>
        </h5>
      )
    }
    return (
      <div className="card-block" key={`result-${this.props.id}`} id={`entry${this.props.id}`}>
        <div hidden={this.state.state !== 'detail'}>
          <div className="pull-xs-right actions">
            {this.props.updateEntry && (
              <a className="btn btn-link btn-sm" onClick={this.editOnClick} href="#">
                <i className="far fa-file-alt" /> Edit
              </a>
            )}
            {this.props.deleteEntry && (
              <a className="btn btn-link btn-sm" onClick={this.deleteOnClick} href="#">
                <i className="fas fa-minus" /> Delete
              </a>
            )}
            {this.props.updateEntry && (
              <a className="btn btn-link btn-sm" onClick={this.addPhotosOnClick} href="#">
                <i className="fas fa-plus" /> Add images
              </a>
            )}
          </div>
          <h5>{Moment(this.props.attributes.service_date, API_FORMAT).format(GUI_FORMAT)}</h5>
          {task}
          <p className="text-muted" style={{whiteSpace: 'pre-wrap'}}>{this.props.attributes.notes}</p>
        </div>

        <form method="POST" action="" onSubmit={this.handleSubmit} hidden={this.state.state !== 'edit'}>
          <div className="form-group required">
            <label className="control-label">Service date</label>
            <div style={{position: 'relative'}}>
              <DateInput
                ref="serviceDate"
                value={Moment(this.state.serviceDate, GUI_FORMAT).format(GUI_FORMAT)}
                placeholder="Service date"
                required
                onChange={this.handleServiceDateChange}
              />
              <div className="alert alert-danger" hidden={this.state.serviceDate.isValid()}>
                <p>Invalid date. Expected DD/MM/YYYY.</p>
              </div>
            </div>
          </div>
          <div className="form-group">
            <label className="control-label">Notes (optional)</label>
            <textarea
              ref="notes"
              className="form-control"
              name="notes"
              placeholder="Notes"
              value={this.state.notes}
              onChange={this.handleNotesChange}
            />
          </div>
          <div className="form-group">
            <label className="control-label">Task (optional)</label>
            <AutocompleteLight
              onChange={this.handleTaskChange}
              value={this.state.task}
              endpoint={'task/?property=' + this.props.propertyId}
              placeholder="Type to search tasks ..."
            />
          </div>
          <div className="pull-xs-right">
            <button className="btn btn-outline-warning btn-sm" onClick={this.cancelEditOnClick}>
              Cancel
            </button>
            &nbsp;
            <button
              type="submit"
              className="btn btn-primary btn-sm"
              disabled={!this.state.serviceDate.isValid()}
            >
              Save
            </button>
          </div>
        </form>

        <div hidden={this.state.state === 'edit'}>
          <FileBrowser
            url={`/api/v2/uploads/logbookentries/${this.props.id}/files/`}

            canDeleteFiles={inEditMode}
            canDeleteFolders={inEditMode}
            canRenameFiles={inEditMode}
            canRenameFolders={inEditMode}
            canMoveFiles={inEditMode}
            canMoveFolders={inEditMode}
            canCreateFiles={inEditMode}
            canCreateFolders={inEditMode}

            renderStyle="list"
            nestChildren
            startOpen
            canFilter={false}
            headerRenderer={null}
            fileRenderer={FileRenderers.ListThumbnailFile}
            folderRenderer={FolderRenderers.ListThumbnailFolder}
          />
        </div>

        <div className="pull-xs-right" hidden={this.state.state !== 'add_photos'}>
          <button className="btn btn-primary btn-sm" onClick={this.cancelEditOnClick}>
            Done
          </button>
        </div>
      </div>
    )
  }
}

class LogbookEntryListPanel extends React.Component {
  static propTypes = {
    url: PropTypes.string,
    logbookId: PropTypes.number,
    propertyId: PropTypes.number,
  }

  state = {
    apiResponse: null,
    entries: [],
  }

  componentDidMount() {
    const url = `${this.props.url}?logbook=${this.props.logbookId}&include=task`
    this.setState({apiResponse: null}, () => {
      ApiUtils.httpGET({
        autoPrefix: false,
        url: url,
        success: (response) => {
          this.setState({apiResponse: ApiUtils.joinIncludes(response)}, () => {
            let entry
            this.setState(prevState => {
              const stateChanges = {
                entries: [...prevState.entries],
              }
              for (let entryIndex = 0; entryIndex < prevState.apiResponse.data.length; entryIndex++) {
                entry = prevState.apiResponse.data[entryIndex]
                entry.id = parseInt(entry.id)
                stateChanges.entries.push(entry)
              }
              return stateChanges
            })
          })
        },
      })
    })
  }

  updateEntry = (entryId, newEntry) => {
    this.setState(prevState => {
      const stateChanges = {
        entries: [...prevState.entries],
      }
      for (let entryIndex = 0; entryIndex < prevState.entries.length; entryIndex++) {
        if (prevState.entries[entryIndex].id === entryId) {
          newEntry.id = parseInt(newEntry.id)
          stateChanges.entries[entryIndex] = newEntry
          break
        }
      }
      return stateChanges
    })
  }

  deleteEntry = (entryId) => {
    this.setState(prevState => {
      const stateChanges = {
        entries: [...prevState.entries],
      }
      for (let entryIndex = 0; entryIndex < prevState.entries.length; entryIndex++) {
        if (prevState.entries[entryIndex].id === entryId) {
          stateChanges.entries.splice(entryIndex, 1)
          break
        }
      }
      return stateChanges
    })
  }

  renderOtherPanels = () => {
    if (this.state.apiResponse === null) {
      return (
        <div className="card-block">
          <p>{ApiUtils.loadingSpinner()} Loading ...</p>
        </div>
      )
    }
    if (this.state.entries.length) {
      const results = []
      let entry
      for (let entryIndex = 0; entryIndex < this.state.entries.length; entryIndex++) {
        entry = this.state.entries[entryIndex]
        const task = entry.relationships.task.data ? {
          value: entry.relationships.task.data.id,
          label: `${entry.relationships.task.data.id}: ${entry.relationships.task.data.attributes.name}`,
        } : null
        results.push(
          <OtherCardBlock
            key={`entry-${entry.id}`}
            url={`${this.props.url + entry.id}/`}
            updateEntry={hasPerm('logbooks.change_logbookentry') ? this.updateEntry : null}
            deleteEntry={hasPerm('logbooks.delete_logbookentry') ? this.deleteEntry : null}
            propertyId={this.props.propertyId}
            task={task}
            {...entry}
          />
        )
      }
      return results
    } else {
      return (
        <div className="card-block">
          <p className="text-muted">No entries</p>
        </div>
      )
    }
  }

  render() {
    return (
      <div className="card">
        <h5 className="card-header">Logbook entries</h5>
        <div className="card-pack">
          {hasPerm('logbooks.add_logbookentry') && (
            <FirstCardBlock
              key="first-panel"
              url={this.props.url}
              logbookId={this.props.logbookId}
              pushNew={this.pushNew}
              propertyId={this.props.propertyId}
            />
          )}
          {this.renderOtherPanels()}
        </div>
      </div>
    )
  }

  pushNew = (entry) => {
    entry.id = parseInt(entry.id)
    this.setState(prevState => ({entries: [entry].concat(prevState.entries)}))
  }
}

export default LogbookEntryListPanel
