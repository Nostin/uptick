import React from 'react';
import { connect } from 'react-redux';
import LoadingSpinner from './components/blocks/Spinner/LoadingSpinner';
import AddNewCard from './components/patterns/AddNewCard/AddNewCard';
import { getData } from './redux/actions/exampleAction';
import { ExampleState } from './redux/reducers/exampleReducer';

interface IProps {
  example: ExampleState;
  rdxGetData(url: string): void;
}

interface IState {
  url: string;
}

class App extends React.Component<IProps, IState> {
  constructor(props: any) {
    super(props);

    this.getData = this.getData.bind(this);
    this.updateUrl = this.updateUrl.bind(this);

    this.state = {
      url: 'https://jsonplaceholder.typicode.com/todos/1',
    };
  }

  public render() {
    const { example } = this.props;
    const { url } = this.state;

    const apiData = JSON.stringify(example.payload);
    const showSpinner = example.apiStatus === 'active';
    const showApiFailMsg = example.apiCallResult === 'fail';
    const showApiSuccessMsg = example.apiCallResult === 'success';

    return (
      <div className="App">
        <AddNewCard logbookId={1000} />

        <input type="text" value={url} onChange={this.updateUrl} />
        <button onClick={this.getData}>call api</button>
        <h4>API Result:</h4>
        {showSpinner && <LoadingSpinner />}
        {showApiFailMsg && <p>Error calling API.</p>}
        {showApiSuccessMsg && <p>Successfully retrieved API data.</p>}
        <p>{apiData}</p>
      </div>
    );
  }

  private getData() {
    const { rdxGetData } = this.props;
    const { url } = this.state;
    rdxGetData(url);
  }

  private updateUrl(event: React.ChangeEvent<HTMLInputElement>) {
    this.setState({ url: event.target.value });
  }
}

const mapStateToProps = (state: { [property: string]: ExampleState }) => ({
  example: state.exampleReducer,
});

const mapDispatchToProps = (dispatch: any) => ({
  rdxGetData: (url: string) => dispatch(getData(url)),
});

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(App);
