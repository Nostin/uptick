import fetchMock from 'fetch-mock';
import configureMockStore from 'redux-mock-store';
import thunk from 'redux-thunk';
import * as actions from '../actions/exampleAction';

const middleware = [thunk];
const mockStore = configureMockStore(middleware);

const goodResponse = {
  status: 200,
  text: 'some data',
};

describe('Performs the API call', () => {
  const apiUrl = 'https://jsonplaceholder.typicode.com/todos/1';

  afterEach(() => {
    fetchMock.restore();
  });

  it('Handles bad response from API', () => {
    fetchMock.once(apiUrl, { throws: 'error' });

    const expectedActions = [{ type: 'API_CALL_BEGIN' }, { type: 'API_CALL_FAIL' }];

    const store = mockStore({});

    return store.dispatch(actions.getData(apiUrl) as any).then(() => {
      expect(store.getActions()).toEqual(expectedActions);
    });
  });

  it('Handles 200 response as expected', () => {
    fetchMock.getOnce(apiUrl, goodResponse);

    const expectedActions = [
      { type: 'API_CALL_BEGIN' },
      { payload: { status: 200, text: 'some data' }, type: 'API_CALL_SUCCESS' },
    ];

    const store = mockStore({});

    return store.dispatch(actions.getData(apiUrl) as any).then(() => {
      expect(store.getActions()).toEqual(expectedActions);
    });
  });
});
