import fetchMock from 'fetch-mock';
import configureMockStore from 'redux-mock-store';
import thunk from 'redux-thunk';
import * as actions from '../actions/newLogbookEntryAction';

const middleware = [thunk];
const mockStore = configureMockStore(middleware);

describe('Performs the API call', () => {
  const url = '/new-logbook-entry-url';

  const goodResponse = {
    status: 200,
    text: 'some data',
  };

  const logbookId: number = 1000;
  const notes: string = 'test notes';
  const serviceDate: Date = new Date();
  const task: string = 'test task';

  afterEach(() => {
    fetchMock.restore();
  });

  it('Handles bad response from API', () => {
    fetchMock.once(url, { throws: 'error' });

    const expectedActions = [
      { type: 'NEW_ENTRY_API_CALL_BEGIN' },
      { type: 'NEW_ENTRY_API_CALL_FAIL' },
    ];

    const store = mockStore({});

    return store
      .dispatch(actions.submitNewLogbookEntry(logbookId, notes, serviceDate, task) as any)
      .then(() => {
        // return of async actions
        expect(store.getActions()).toEqual(expectedActions);
      });
  });

  it('Handles 200 good response as expected', () => {
    fetchMock.once(url, { body: goodResponse, status: 200 });

    const expectedActions = [
      { type: 'NEW_ENTRY_API_CALL_BEGIN' },
      { payload: { status: 200, text: 'some data' }, type: 'NEW_ENTRY_API_CALL_SUCCESS' },
    ];

    const store = mockStore({});

    return store
      .dispatch(actions.submitNewLogbookEntry(logbookId, notes, serviceDate, task) as any)
      .then(() => {
        // return of async actions
        expect(store.getActions()).toEqual(expectedActions);
      });
  });
});
