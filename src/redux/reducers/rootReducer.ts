import { combineReducers } from 'redux';
import exampleReducer from './exampleReducer';
import newLogbookEntryReducer from './newLogbookEntryReducer';

export default combineReducers({
  exampleReducer,
  newLogbookEntryReducer,
});
