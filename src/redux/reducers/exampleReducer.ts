import update from 'immutability-helper';

export interface ExampleState {
  apiCallResult: 'none' | 'success' | 'fail';
  apiStatus: 'active' | 'dormant';
  payload: object;
  type: string;
}

export default (
  state = {
    apiCallResult: 'none',
    apiStatus: 'dormant',
    payload: {},
  },
  action: ExampleState
) => {
  switch (action.type) {
    case 'API_CALL_BEGIN':
      return update(state, {
        apiCallResult: { $set: 'none' },
        apiStatus: { $set: 'active' },
      });
    case 'API_CALL_FAIL':
      return update(state, {
        apiCallResult: { $set: 'fail' },
        apiStatus: { $set: 'dormant' },
        payload: { $set: {} },
      });
    case 'API_CALL_SUCCESS':
      return update(state, {
        apiCallResult: { $set: 'success' },
        apiStatus: { $set: 'dormant' },
        payload: { $set: action.payload },
      });
    default:
      return state;
  }
};
