import update from 'immutability-helper';

export interface NewLogbookEntryState {
  apiCallResult: 'none' | 'success' | 'fail';
  apiStatus: 'active' | 'dormant';
  notes: string;
  serviceDate: Date;
  state: 'new' | 'create';
  task: string;
  taskOptions: [{ label: string; key: string }];
  type: string;
}

export default (
  state = {
    apiCallResult: 'none',
    apiStatus: 'dormant',
    notes: '',
    serviceDate: new Date(),
    state: 'new',
    task: '',
    taskOptions: [
      { key: 'id1', label: 'Task One' },
      { key: 'id2', label: 'Task Two' },
      { key: 'id3', label: 'Task Three' },
    ],
  },
  action: NewLogbookEntryState
) => {
  switch (action.type) {
    case 'SHOW_CREATE':
      return update(state, {
        apiCallResult: { $set: 'none' },
        state: { $set: 'create' },
      });
    case 'SHOW_NEW':
      return update(state, {
        apiCallResult: { $set: 'none' },
        state: { $set: 'new' },
      });
    case 'UPDATE_SERVICE_DATE':
      return update(state, {
        serviceDate: { $set: action.serviceDate },
      });
    case 'UPDATE_NOTES':
      return update(state, {
        notes: { $set: action.notes },
      });
    case 'UPDATE_TASK':
      return update(state, {
        task: { $set: action.task },
      });
    case 'NEW_ENTRY_API_CALL_BEGIN':
      return update(state, {
        apiCallResult: { $set: 'none' },
        apiStatus: { $set: 'active' },
      });
    case 'NEW_ENTRY_API_CALL_FAIL':
      return update(state, {
        apiCallResult: { $set: 'fail' },
        apiStatus: { $set: 'dormant' },
      });
    case 'NEW_ENTRY_API_CALL_SUCCESS':
      return update(state, {
        apiCallResult: { $set: 'success' },
        apiStatus: { $set: 'dormant' },
      });
    default:
      return state;
  }
};
