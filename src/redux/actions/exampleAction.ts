import 'cross-fetch/polyfill';

const apiCallBegin = () => ({
  type: 'API_CALL_BEGIN',
});

const apiCallSuccess = (payload: object) => ({
  payload,
  type: 'API_CALL_SUCCESS',
});

const apiCallFail = () => ({
  type: 'API_CALL_FAIL',
});

export const getData = (url: string) => (dispatch: any) => {
  dispatch(apiCallBegin());
  return fetch(url)
    .then(response => response.json())
    .then(body => {
      dispatch(apiCallSuccess(body));
    })
    .catch(err => {
      dispatch(apiCallFail());
    });
};
