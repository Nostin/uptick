import 'cross-fetch/polyfill';

export const showCreate = () => ({
  type: 'SHOW_CREATE',
});

export const showNew = () => ({
  type: 'SHOW_NEW',
});

export const updateLogBookEntryServiceDate = (serviceDate: Date) => ({
  serviceDate,
  type: 'UPDATE_SERVICE_DATE',
});

export const updateLogBookEntryNotes = (notes: string) => ({
  notes,
  type: 'UPDATE_NOTES',
});

export const updateLogBookEntryTask = (task: string) => ({
  task,
  type: 'UPDATE_TASK',
});

const apiNewEntryBegin = () => ({
  type: 'NEW_ENTRY_API_CALL_BEGIN',
});

const apiNewEntrySuccess = (payload: object) => ({
  payload,
  type: 'NEW_ENTRY_API_CALL_SUCCESS',
});

const apiNewEntryFail = () => ({
  type: 'NEW_ENTRY_API_CALL_FAIL',
});

export const submitNewLogbookEntry = (
  logbookId: number,
  notes: string,
  serviceDate: Date,
  task: string
) => (dispatch: (action: object) => void) => {
  dispatch(apiNewEntryBegin());

  const payload = {
    attributes: {
      notes,
      service_date: serviceDate.toString(),
    },
    relationships: {
      logbook: {
        data: {
          id: logbookId,
          type: 'Logbook',
        },
      },
      task: {
        data: task,
      },
    },
    type: 'LogbookEntry',
  };

  const postData: RequestInit = {
    body: JSON.stringify(payload),
    headers: {
      Accept: 'application/json',
      'Content-Type': 'application/vnd.api+json',
    },
    method: 'POST',
  };

  return fetch('/new-logbook-entry-url', postData)
    .then(response => response.json())
    .then(body => {
      dispatch(apiNewEntrySuccess(body));
    })
    .catch(err => {
      dispatch(apiNewEntryFail());
    });
};
